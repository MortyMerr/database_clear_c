#include "damages_t.h"
#include <string.h>
#include <stdlib.h>
#include "memstat.h"

int verifyDamage(const char * s)
{
  int i = 0;
  while ((i < setSize)
    && (strcmp(damageVars[i], s) != 0)) {
    i++;
  }
  if (i == setSize) {
    return -1;
  }
  return i;
}

int compareDamage(struct set_damages_t lhs, struct set_damages_t rhs)
{
  int i = 0, counter = 0, equal = 1;
  for (; i < setSize; i++) {
    if (lhs.damages[i] != rhs.damages[i]) {
      equal = 0;
      if (lhs.damages[i] < rhs.damages[i]) {
        counter++;
      }
      else {
        counter--;
      }
    }
  }
  if (equal) {
    return 0;
  }

  if (counter < 0) {
    return 1;
  }
  if (counter > 0) {
    return -1;
  }
  return -1000; //error. Uncomparable objects
}
int inDamage(struct set_damages_t lhs, struct set_damages_t rhs)
{
  int i = 0;
  while ((i < setSize)
    && (lhs.damages[i] <= rhs.damages[i])) {
    i++;
  }
  return (i == setSize);
}

int includeDamage(struct set_damages_t lhs, struct set_damages_t rhs)
{
  return inDamage(rhs, lhs);
}

struct set_damages_t make_damage(const char* s, int* error)
{
  struct set_damages_t tmp;
  char* parse = NULL;
  tmp.damages = (int*)malloc(sizeof(int) * setSize);
  mallocCnt++;
  tmp.size = 0;
  parse = strtok(s, "'");
  while ((parse = strtok(NULL, "',]")) != NULL) {
    const int num = verifyDamage(parse);
    if (num == -1) {
      free(tmp.damages);
      freeCnt++;
      return tmp;
    }
    tmp.size++;
    tmp.damages[num] = 1;
  }
  *error = 1;
  return tmp;
}

void printDamages(FILE* fout, struct set_damages_t s)
{
  int i = 0, counter = 0;
  fprintf(fout, "occ_damage=[");
  for (; i < setSize; i++) {
    if (s.damages[i] == 1) {
      counter++;
      fprintf(fout, "\'%s\'", damageVars[i]);
      if (counter < s.size) {
        fprintf(fout, ",");
      }
    }
  }
  //FIXME ��������� �������
  fprintf(fout, "]");
}
