#ifndef DAMAGES_T_H
#define DAMAGES_T_H
#define setSize 8

#include <stdio.h>
#include "string_t.h"

static const char* damageVars[setSize] = {  "bonnet", "bumper",
"door", "engine", "glass", "rear-door", "top",  "wing"};

struct set_damages_t {
  int* damages;
  int size;
};

//-1 - not found or numnber of enum
int verifyDamage(const char * s);

struct set_damages_t make_damage(const char* s, int* error);

//0 - lhs == rhs, 1 lhs > rhs, -1 lhs < rhs, -1000 - uncomparable
int compareDamage(struct set_damages_t lhs, struct set_damages_t rhs);

//1 - in, 0 - no;
int inDamage(struct set_damages_t lhs, struct set_damages_t rhs);

//1 - include, 0 - no;
int includeDamage(struct set_damages_t lhs, struct set_damages_t rhs);

void printDamages(FILE* fout, struct set_damages_t s);

#endif // !DAMAGES_T_H
