#ifndef CARNUM_T_H
#define CARNUM_T_H
#include "string_t.h"
#include <stdio.h>

struct carnum_t {
  char* LLL;
  int DDD;
  int RRR;
};

//0 - lhs == rhs, -1 lhs < rhs; 1 - lhs > rhs
int compareCarNum(struct carnum_t lhs, struct carnum_t rhs);

struct carnum_t make_car_id(const char* s, int* error);

void printCarNum(FILE* fout, struct carnum_t num);

int verifyCarNum(struct carnum_t carnum);

#endif // !CURNUM_T_H
