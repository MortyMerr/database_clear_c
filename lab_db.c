#include <stdio.h>
#include <stdlib.h>
#include "bd_t.h"
#include "func.h"
#define NO_FILE 1
#define readStringSize 500

int callocCnt = 0,
mallocCnt = 0,
freeCnt = 0,
reallocCnt = 0;

void writeMemStat()
{
  FILE* fout = fopen("memstat.txt", "w");
  fprintf(fout, "malloc:%d\n", mallocCnt);
  fprintf(fout, "realloc:%d\n", reallocCnt);
  fprintf(fout, "calloc:%d\n", callocCnt);
  fprintf(fout, "free:%d\n", freeCnt);
}

int main(void) {
  FILE* fin = fopen("files/input.txt", "r");
  FILE* fout = fopen("files/output.txt", "w");
  char* tmpString = NULL;
  struct list_t* head = (struct list_t*)malloc(sizeof(struct list_t)),
    *current = head;
  int* flagError = (int*)malloc(sizeof(int));
  int bdSize = 0;
  mallocCnt += 2;
  head->next = NULL;


  if (fin == NULL) {
    printf("Error: no input.txt found\n");
    return NO_FILE;
  }

  while ((tmpString = readString(fin)) != EOF) {
    if (tmpString == NULL) {
      continue;
    }
    char* parseTmp = (char*)malloc(sizeof(char) * strlen(tmpString)),
      *token = NULL, *last = NULL,
      *command = NULL;
    mallocCnt++;

    strcpy(parseTmp, tmpString);
    command = strtok_s(parseTmp, " ", &last);
    switch (commandSwithc(command)) {
    case -1:
    error:fprintf(fout, "incorrect:'");
      tmpString[20] = '\0';
      fprintf(fout, tmpString);
      fprintf(fout, "\'\n");
      break;
    case 0://insert
    {
      struct inf tmp;
      char bufer[100];
      bufer[0] = '\0';
      command = strtok_s(NULL, " =", &last);
      while (command != NULL) {
        if (*flagError == 0) {
          goto error;
        }
        const int commandNum = columnSwitch(command);
        if (commandNum == 6) {
          command = strtok_s(NULL, "]\n", &last);
        }
        else {
          command = strtok_s(NULL, ",\n", &last);
          bufer[0] = '\0';
          strcat(bufer, command);
          if (last != NULL
            && last[0] != '\0'
            && strchr("fmlcio", last[0]) == NULL) {
            command = strtok_s(NULL, ",\n", &last);
            strcat(bufer, ",");
            strcat(bufer, command);
          }
        }
        *flagError = 0;
        switch (commandNum) {
        case 0://id
          sscanf(command, "%d", &tmp.id);
          *flagError = 1;
          break;
        case 1://first_name
          tmp.first_name.inf = (char*)malloc(strlen(bufer));
          mallocCnt++;
          strcpy(tmp.first_name.inf, bufer);
          *flagError = 1;
          break;
        case 2://middle_name
          tmp.middle_name.inf = (char*)malloc(strlen(bufer));
          mallocCnt++;
          strcpy(tmp.middle_name.inf, bufer);
          *flagError = 1;
          break;
        case 3://last_name
          tmp.last_name.inf = (char*)malloc(strlen(bufer));
          mallocCnt++;

          strcpy(tmp.last_name.inf, bufer);
          *flagError = 1;
          break;
        case 4://car_id
          tmp.car_id = make_car_id(command, flagError);
          break;
        case 5://occ_date
          tmp.occ_date = make_date(command, flagError);
          break;
        case 6://occ_damage
          tmp.occ_damage = make_damage(command, flagError);
          if (last[0] != '\0') {
            last++;
          }
          break;
        default:
          goto error;
        }
        command = strtok_s(NULL, "=", &last);
      }
      current->member = tmp;
      current->my_id = bdSize;
      current->next = (struct list_t*)malloc(sizeof(struct list_t));
      mallocCnt++;
      bdSize++;
      fprintf(fout, "insert:%d\n", bdSize);
      current = current->next;
      current->next = NULL;
    }
    break;
    case 1://select
    {
      int* out = (int*)malloc(sizeof(int) * 7),
        *cond = (int*)malloc(sizeof(int) * bdSize),
        counter = 0,
        *countOfOkMembers = (int*)malloc(sizeof(int));
      mallocCnt += 3;
      *countOfOkMembers = bdSize;
      struct list_t* selectTmp = head;
      command = strtok_s(NULL, " ,\n", &last);
      while (command != NULL) {
        char* res = strstr(last, operations);
        if (findSymbols(command, operations) != NULL)//condition
        {
          do {
            *flagError = 0;
            sheduleCondtion(command, head, cond, countOfOkMembers, flagError);
            if (*flagError != 0) {
              goto error;
            }
            command = strtok_s(NULL, " \n", &last);
          } while (command != NULL);
        }
        else {
          const int result = columnSwitch(command);
          if (result == -1) {
            goto error;
          }
          out[counter++] = result;
        }
        command = strtok_s(NULL, " ,\n", &last);
      }
      selectTmp = head;
      fprintf(fout, "select:%d\n", *countOfOkMembers);
      while (selectTmp->next != NULL) {
        if (cond[selectTmp->my_id] != 0) {
          fprintf(fout, "\n");
          printMember(fout, out, &selectTmp->member);
          fprintf(fout, "\n");
        }
        selectTmp = selectTmp->next;
      }
      break;
    }
    case 2://delete
    {
      int *cond = (int*)malloc(sizeof(int) * bdSize),
        counter = 0,
        *countOfOkMembers = (int*)malloc(sizeof(int));
      mallocCnt += 2;
      *countOfOkMembers = bdSize;
      struct list_t* selectTmp = head;
      command = strtok_s(NULL, " \n", &last);
      do {
        *flagError = 0;
        sheduleCondtion(command, head, cond, countOfOkMembers, flagError);
        if (*flagError != 0) {
          goto error;
        }
        command = strtok_s(NULL, " \n", &last);
      } while (command != NULL);

      fprintf(fout, "delete:%d\n", *countOfOkMembers);
      bdSize -= *countOfOkMembers;
      while (selectTmp->next != NULL) {
        if (cond[selectTmp->my_id] != 0) {
          struct list_t* deleter = head;
          if (deleter == selectTmp) {
            head = head->next;
            selectTmp = selectTmp->next;
            free(deleter);
          }
          else {
            while (deleter->next != selectTmp) {
              deleter = deleter->next;
            }
            deleter->next = selectTmp->next;
            free(selectTmp);
            selectTmp = deleter;
          }
        }
        else {
          selectTmp = selectTmp->next;
        }
      }
      //update_my_id
      selectTmp = head;
      counter = 0;
      while (selectTmp->next != NULL) {
        selectTmp->my_id = counter++;
        selectTmp = selectTmp->next;
      }
      break;
    }
    case 3://update
    {
      struct inf tmp;
      int update[6];
      *flagError = 1;
      char bufer[100];
      bufer[0] = '\0';
      command = strtok_s(NULL, " =", &last);
      while (command != NULL) {
        if (*flagError == 0) {
          goto error;
        }
        const int commandNum = columnSwitch(command);
        if (commandNum == 6) {
          command = strtok_s(NULL, "]\n", &last);
        }
        else {
          command = strtok_s(NULL, " ,\n", &last);
          bufer[0] = '\0';
          strcat(bufer, command);
          if (last != NULL
            && last[0] != '\0'
            && strchr("fmlcio ", last[0]) == NULL) {
            command = strtok_s(NULL, ",\n", &last);
            strcat(bufer, ",");
            strcat(bufer, command);
          }
        }
        *flagError = 0;
        if (commandNum == -1) {
          int i = 0;
        }
        update[commandNum] = 0;
        switch (commandNum) {
        case 0://id
          sscanf(command, "%d", &tmp.id);
          *flagError = 1;
          break;
        case 1://first_name
          tmp.first_name.inf = (char*)malloc(strlen(bufer));
          mallocCnt++;
          strcpy(tmp.first_name.inf, bufer);
          *flagError = 1;
          break;
        case 2://middle_name
          tmp.middle_name.inf = (char*)malloc(strlen(bufer));
          mallocCnt++;
          strcpy(tmp.middle_name.inf, bufer);
          *flagError = 1;
          break;
        case 3://last_name
          tmp.last_name.inf = (char*)malloc(strlen(bufer));
          mallocCnt++;
          strcpy(tmp.last_name.inf, bufer);
          *flagError = 1;
          break;
        case 4://car_id
          tmp.car_id = make_car_id(command, flagError);
          break;
        case 5://occ_date
          tmp.occ_date = make_date(command, flagError);
          break;
        case 6://occ_damage
          tmp.occ_damage = make_damage(command, flagError);
          if (last[0] != '\0') {
            last++;
          }
          break;
        default:
          goto error;
        }
        command = strtok_s(NULL, " \n", &last);
        if (findSymbols(command, operations) != NULL)//condition
        {
          break;
        }
      }
      int *cond = (int*)malloc(sizeof(int) * bdSize),
        counter = 0,
        *countOfOkMembers = (int*)malloc(sizeof(int));
      mallocCnt += 2;
      *countOfOkMembers = 0;
      struct list_t* selectTmp = head;
      do {
        *flagError = 0;
        sheduleCondtion(command, head, cond, countOfOkMembers, flagError);
        if (*flagError != 0) {
          goto error;
        }
        command = strtok_s(NULL, " \n", &last);
      } while (command != NULL);

      fprintf(fout, "update:%d\n", bdSize + *countOfOkMembers);
      selectTmp = head;
      while (selectTmp->next != NULL) {
        if (cond[selectTmp->my_id] != 0) {
          int i = 0;
          for (i = 0; i < 6; i++) {
            if (update[i] == 0) {
              switch (i) {
              case 0://id
                selectTmp->member.id = tmp.id;
                break;
              case 1://first_name
                selectTmp->member.first_name = tmp.first_name;
                break;
              case 2://middle_name
                selectTmp->member.middle_name = tmp.middle_name;
                break;
              case 3://last_name
                selectTmp->member.last_name = tmp.last_name;
                break;
              case 4://car_id
                selectTmp->member.car_id = tmp.car_id;
                break;
              case 5://occ_date
                selectTmp->member.occ_date = tmp.occ_date;
                break;
              case 6://occ_damage
                selectTmp->member.occ_damage = tmp.occ_damage;
              }
            }
          }
        }
        selectTmp = selectTmp->next;
      }
      break;
    }
    }
    free(tmpString);
    freeCnt += 2;
    fprintf(fout, "\n");
  }

  current = head;
  /*while (current->next != NULL) {
    free(current)
  }*/
  fclose(fin);
  fclose(fout);
  writeMemStat();
  return 0;
}