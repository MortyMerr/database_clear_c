#ifndef DB_T_H
#define DB_T_H

#include <stdio.h>
#include "date_t.h"
#include "string_t.h"
#include "carnum_t.h"
#include "damages_t.h"

static const char * operations = "<>=!/";
static const char * includeOp = "include";
static const char * inOp = "in";


struct inf {
  int id;
  struct string_t first_name, last_name, middle_name;
  struct carnum_t car_id;
  struct date_t occ_date;
  struct set_damages_t occ_damage;
};

struct list_t {
  struct inf member;
  int my_id;
  struct list_t* next;
};

void printMember(FILE* fout, int * out, struct inf* m);
void printId(FILE* fout, int id);
void sheduleCondtion(char* con, struct list_t* head, int* ok, int* size, int* error);
char* findSymbols(const char* source, const char* delim);
int compareId(int lhs, int rhs);
char* delSpaces(char* s);
#endif // !DB_T_H
