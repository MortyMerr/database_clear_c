#ifndef DATE_T_H
#define DATE_T_H

#include <stdio.h>

struct date_t {
  int day, month, year;
};

//0 - equals, -1 lhs < rhs, 1 lhs > rhs
int compareDate(struct date_t lhs, struct date_t rhs);

//0 - error, 1 - ok
int verifyDate(struct date_t date);

struct date_t make_date(const char* s, int* error);

void printDate(FILE* fout, struct date_t date);

#endif // !DATE_T_H