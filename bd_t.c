#include "bd_t.h"
#include "func.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printMember(FILE* fout, int * out, struct inf* m)
{
  int i = 0;
  for (; i < 7; i++) {
    switch (out[i]) {
    case 0:
      fprintf(fout, "id=%d", m->id);
      break;
    case 1:
      fprintf(fout, "first_name=%s", m->first_name.inf);
      break;
    case 2:
      fprintf(fout, "middle_name=%s", m->middle_name.inf);
      break;
    case 3:
      fprintf(fout, "last_name=%s", m->last_name.inf);
      break;
    case 4:
      printCarNum(fout, m->car_id);
      break;
    case 5:
      printDate(fout, m->occ_date);
      break;
    case 6:
      printDamages(fout, m->occ_damage);;
      break;
    }
    fprintf(fout, " ");
  }
}

void sheduleCondtion(char* con, struct list_t* head, int* ok, int* size, int* error)
{
  char* last = NULL, *command = NULL,
    *remember = (char*)malloc(sizeof(char));
  int opNum = -1;
  *remember = findSymbols(con, operations)[0];
  remember[1] = '\0';
  con = delSpaces(con);
  command = strtok_s(con, operations, &last);
  const int commandNum = columnSwitch(command);
  if (commandNum == 0) {
    if (strchr(operations, last[0])) {
      char* bufer = (char*)malloc(3);
      bufer[0] = '\0';
      strcat(bufer, remember);
      strcat(bufer, &last[0]);
      bufer[2] = '\0';
      opNum = compareSwitch(bufer);
    }
    command = strtok_s(NULL, operations, &last);
  }
  else if (commandNum == 6) {
    command = strtok_s(NULL, "/[", &last);//������
  }
  else {
    command = strtok_s(NULL, " \"'", &last);
  }

  if ((strstr(command, includeOp) != NULL)
    || strstr(command, inOp) != NULL) {
    opNum = compareSwitch(command);
    command = strtok_s(NULL, "/", &last);
  }
  else
    if (strchr(operations, command[0]) != NULL) {
      opNum = compareSwitch(strcat(remember, command));
      command = strtok_s(NULL, " ", &last);
    }
    else
    {
      opNum = compareSwitch(remember);
    }

  if (opNum == -1) {
    return;
  }
  if (commandNum == -1) {
    return;
  }
  while (head->next != NULL) {
    switch (commandNum) {
    case 0://id
    {
      const int condResult = compareId(head->member.id, atoi(command));
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult == 1) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult == -1) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }

      break;
    }
    case 1://first_name
    {
      char* bufer = (char*)malloc(strlen(command) + 2);
      bufer[0] = '\"';
      bufer[1] = '\0';
      strcat(bufer, command);
      const int condResult = strcmp(head->member.first_name.inf, bufer);
      free(bufer);
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult > 0) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult < 0) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }
      break;
    }
    case 2://middle_name
    {
      char* bufer = (char*)malloc(strlen(command) + 2);
      bufer[0] = '\"';
      bufer[1] = '\0';
      strcat(bufer, command);
      const int condResult = strcmp(head->member.middle_name.inf, bufer);
      free(bufer);
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult > 0) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult < 0) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }
      break;
    }
    case 3://last_name
    {
      char* bufer = (char*)malloc(strlen(command) + 2);
      bufer[0] = '\"';
      bufer[1] = '\0';
      strcat(bufer, command);
      const int condResult = strcmp(head->member.last_name.inf, bufer);
      free(bufer);
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult > 0) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult < 0) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }
      break;
    }
    case 4://car_id
    {
      char* bufer = (char*)malloc(strlen(command) + 2);
      bufer[0] = '\"';
      bufer[1] = '\0';
      strcat(bufer, command);
      const struct carnum_t tmpCar = make_car_id(bufer, error);
      if (*error == 1) {
        *error = 0;
      }
      free(bufer);
      const int condResult = compareCarNum(head->member.car_id, tmpCar);
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult == 1) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult == -1) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }
      break;
    }
    case 5://occ_date
    {
      char* bufer = (char*)malloc(strlen(command) + 2);
      bufer[0] = '\"';
      bufer[1] = '\0';
      strcat(bufer, command);
      const struct date_t tmpDate = make_date(bufer, error);
      if (*error == 1) {
        *error = 0;
      }
      free(bufer);
      const int condResult = compareDate(head->member.occ_date, tmpDate);
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult == 1) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult == -1) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }
      break;
    }
    case 6://occ_damage
    {
      char* bufer = (char*)malloc(strlen(command) + 2);
      bufer[1] = '[';
      bufer[1] = '\0';
      strcat(bufer, command);
      const struct set_damages_t tmpDamage = make_damage(bufer, error);
      if (*error == 1) {
        *error = 0;
      }
      free(bufer);
      int includeIn = 0;
      if (opNum == 7) {
        includeIn = inDamage(head->member.occ_damage, tmpDamage);
      }
      else if (opNum == 8) {
        includeIn = includeDamage(head->member.occ_damage, tmpDamage);
      }
      const int condResult = compareDamage(head->member.occ_damage, tmpDamage);
      const int equal = ((condResult == 0) && ((opNum == 0) || (opNum == 3) || (opNum == 4))),
        bigger = ((condResult == 1) && ((opNum == 2) || (opNum == 3) || (opNum == 6))),
        smaller = ((condResult == -1) && ((opNum == 1) || (opNum == 4) || (opNum == 6)));
      if ((!(equal || bigger || smaller || includeIn))
        && (ok[head->my_id] != 0)) {
        ok[head->my_id] = 0;
        *size = *size - 1;
      }
      break;
    }
    }
    head = head->next;
  }
}

char* findSymbols(const char* source, const char* delim)
{
  const int size = strlen(delim);
  int i = 0, count = strlen(source);
  for (; i < size; i++) {
    if (strchr(source, delim[i]) != NULL) {
      if ((strchr(source, delim[i]) - source) < count) {
        count = strchr(source, delim[i]) - source;
      }
    }
  }
  if (count == strlen(source)) {
    return NULL;
  }
  return (source + count);
}

char* delSpaces(char* s)
{
  char* cpy = (char*)malloc(sizeof(int) * strlen(s));
  int i, j;
  for (i = j = 0; s[i] != '\0'; i++) {
    if (s[i] != ' ') {
      cpy[j++] = s[i];
    }
  }
  cpy[j] = '\0';
  return cpy;
}

int compareId(int lhs, int rhs)
{
  if (lhs == rhs) {
    return 0;
  }

  if (lhs > rhs) {
    return 1;
  }

  return -1;
}