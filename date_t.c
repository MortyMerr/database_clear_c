#include <string.h>
#include <stdio.h>
#include "date_t.h"

int compareDate(struct date_t lhs, struct date_t rhs)
{
  if ((lhs.year == rhs.year)
    && (lhs.month == rhs.month)
    && (lhs.day == rhs.day))
  {
    return 0;
  }
  if ((lhs.year < rhs.year)
    || ((lhs.year == rhs.year) && (lhs.month < rhs.month))
    || ((lhs.year == rhs.year) && (lhs.month == rhs.month) && (lhs.day < rhs.day)))
  {
    return -1;
  }
  return 1;
}

int verifyDate(struct date_t d)
{
  if ((d.year < 0)
    || (d.month < 0)
    || (d.day < 0))
  {
    return 0;
  }

  if ((d.month > 12)
    || (d.day > 31))
  {
    return 0;
  }

  if ((d.month == 2)
    && (d.day > 28))
  {
    if ((d.month / 4) != 0) {
      return 0;
    }
  }

  if (((d.month == 4)
    || (d.month == 6)
    || (d.month == 9)
    || (d.month == 11))
    && (d.day > 30))
  {
    return 0;
  }
  return 1;
}

struct date_t make_date(const char* s, int* error) 
{
  struct date_t tmp;
  char* parse = NULL, * tmpString = s;
  tmpString++;
  parse = strtok(tmpString, ".");
  sscanf(parse, "%d", &tmp.day);
  parse = strtok(NULL, ".");
  sscanf(parse, "%d", &tmp.month);
  parse = strtok(NULL, "'");
  sscanf(parse, "%d", &tmp.year);
  *error = verifyDate(tmp);
  return tmp;
}

void printDate(FILE* fout, struct date_t date)
{
  fprintf(fout, "occ_date='");
  if (date.day < 10) {
    fprintf(fout, "0");
  }
  fprintf(fout, "%d.", date.day);
  if (date.month < 12) {
    fprintf(fout, "0");
  }
  fprintf(fout, "%d.", date.month);
  fprintf(fout, "%d'", date.year);
}