#include "func.h"
#include <string.h>
#include <stdlib.h>
#include "memstat.h"

int commandSwithc(const char* string)
{
  if (strcmp(string, "insert") == 0) {
    return 0;
  }

  if (strcmp(string, "select") == 0) {
    return 1;
  }

  if (strcmp(string, "delete") == 0) {
    return 2;
  }

  if (strcmp(string, "update") == 0) {
    return 3;
  }

  if (strcmp(string, "uniq") == 0) {
    return 4;
  }

  if (strcmp(string, "sort") == 0) {
    return 5;
  }

  return -1;
}

char* readString(FILE* fin)
{
  unsigned int N = 20, i = 0;
  const unsigned int delta = 10;
  char* buf = (char*)malloc(sizeof(char)*N);
  mallocCnt++;
  while (((buf[i] = fgetc(fin)) != '\n')
    && (buf[i] != EOF)) {
    if (++i >= N) {
      N += delta;
      buf = (char*)realloc(buf, sizeof(char)*N);
      reallocCnt++;
    }
  }
  if (buf[0] == EOF) {
    return EOF;
  }
  if (i == 0) {
    return NULL;
  }
  buf[i] = '\0';
  return buf;
}

int columnSwitch(const char* string)
{
  if (strcmp(string, "id") == 0) {
    return 0;
  }

  if (strcmp(string, "first_name") == 0) {
    return 1;
  }

  if (strcmp(string, "middle_name") == 0) {
    return 2;
  }

  if (strcmp(string, "last_name") == 0) {
    return 3;
  }

  if (strcmp(string, "car_id") == 0) {
    return 4;
  }

  if (strcmp(string, "occ_date") == 0) {
    return 5;
  }

  if (strcmp(string, "occ_damage") == 0) {
    return 6;
  }
  return -1;
}

int compareSwitch(const char* string)
{
  if (strcmp(string, "==") == 0) {
    return 0;
  }

  if (strcmp(string, "<") == 0) {
    return 1;
  }

  if (strcmp(string, ">") == 0) {
    return 2;
  }

  if (strcmp(string, ">=") == 0) {
    return 3;
  }

  if (strcmp(string, "<=") == 0) {
    return 4;
  }

  if (strcmp(string, "/") == 0) {
    return 5;
  }

  if (strcmp(string, "!=") == 0) {
    return 6;
  }

  if (strcmp(string, "in") == 0) {
    return 7;
  }

  if (strcmp(string, "include") == 0) {
    return 8;
  }

  return -1;
}
