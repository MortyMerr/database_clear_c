#ifndef FUNC_H
#define FUNC_H
#include <stdio.h>

int commandSwithc(const char* string);
int columnSwitch(const char* string);
int compareSwitch(const char* string);
char* readString(FILE* fin);
#endif // !FUNC_H
