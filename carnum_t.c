#include "carnum_t.h"
#include <stdlib.h>
#include <stdio.h>
#include "memstat.h"

int compareCarNum(struct carnum_t lhs, struct carnum_t rhs)
{
  if (lhs.DDD != rhs.DDD) {
    if (lhs.DDD > rhs.DDD) {
      return 1;
    }
    else {
      return -1;
    }
  }

  const int cmpLLL = strcmp(lhs.LLL, rhs.LLL);
  if (cmpLLL != 0) {
    return cmpLLL;
  }

  if (lhs.RRR != rhs.RRR) {
    if (lhs.RRR > rhs.RRR) {
      return 1;
    }
    else {
      return -1;
    }
  }

  return 0;
}

int verifyCarNum(struct carnum_t carnum)
{
  //FIXME
  return 0;
}


struct carnum_t make_car_id(const char* s, int* error)
{
  struct carnum_t tmp;
  char buf[3];
  tmp.LLL = (char*)malloc(sizeof(char) * 4);
  mallocCnt++;
  if (strlen(s) < 9) {
    free(tmp.LLL);
    freeCnt++;
    return tmp;
  }
  tmp.LLL[0] = s[1];
  tmp.LLL[1] = s[5];
  tmp.LLL[2] = s[6];
  tmp.LLL[3] = '\0';

  buf[0] = s[2];
  buf[1] = s[3];
  buf[2] = s[4];
  sscanf(buf, "%d", &tmp.DDD);
  buf[0] = s[7];
  buf[1] = s[8];

  if (strlen(s) == 11) {
    buf[2] = s[9];
  }
  else buf[2] = '\0';
  sscanf(buf, "%d", &tmp.RRR);
  //TODO maybe verify number; check error somehow
  *error = 1;
  return tmp;
}

void printCarNum(FILE* fout, struct carnum_t car)
{
  fprintf(fout, "car_id='%c", car.LLL[0]);
  if (car.DDD < 10) {
    fprintf(fout, "00%d", car.DDD);
  }
  else if (car.DDD < 100) {
    fprintf(fout, "0%d", car.DDD);
  }
  else
  {
    fprintf(fout, "%d", car.DDD);
  }
  fprintf(fout, "%c%c", car.LLL[1], car.LLL[2]);
  if (car.RRR < 10) {
    fprintf(fout, "0%d'", car.RRR);
  }
  else {
    fprintf(fout, "%d'", car.RRR);

  }
}
