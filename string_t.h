#ifndef STRING_T_H
#define STRING_T_H

#include <string.h>

struct string_t {
  const char* inf;
};

//0 - lhs == rhs, negative lhs < rhs, positive lhs > rhs
int compareString(struct string_t lhs, struct string_t rhs);

#endif // !STRING_T_H
